#pragma once
#include "doodle/image.hpp" // doodle::image
#include "BasicDataTypes.h" // Vector2D, Vector2DInt
#include "TransformMatrix.h"//translateMatrix;


class Texture 
{
	doodle::Image image;
	Vector2DInt size;
public:
	Texture();
	Texture(const std::filesystem::path& filePath);
	void Load(const std::filesystem::path& filePath);
	void Draw(TransformMatrix displayMatrix);
	Vector2DInt GetSize();
};
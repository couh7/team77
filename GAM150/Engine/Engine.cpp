#include <chrono>                //std::chrono::system_clock::now()

#include "GameStateManager.h"
#include "Engine.h"

const double Engine::targetFPS = 30.0;

Engine::Engine() : gameStateManager() {}

void Engine::Init(std::string windowName) 
{
	GetWindow().Init(windowName);
	lastTick = std::chrono::system_clock::now();
}

void Engine::Update() 
{
	std::chrono::system_clock::time_point currTime = std::chrono::system_clock::now();
	double deltaTime = std::chrono::duration<double>(currTime - lastTick).count();

	if (deltaTime < (1.0 / targetFPS)) 
	{ 
		return; 
	}
	lastTick = currTime;


	GetGameStateManager().Update(deltaTime);
	GetInput().Update();
	GetWindow().Update();
}
void Engine::Shutdown() 
{
}
bool Engine::HasGameEnded() 
{
	return GetGameStateManager().HasGameEnded();
}

Engine::~Engine() {
}


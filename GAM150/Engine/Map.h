#pragma once

#define TILE_WIDTH 25
#define TILE_HEIGHT 25

class PrototypeLevel;
class [[nodiscard]] Map
{
public:
    enum class TileState
    {
        BLANK = 0,
        WALL,
        MOUSE,
        TRAP,
        BOX,
        CHEESE,
    };
    
    Map(int winWidth, int winHeight);

    ~Map(){
        delete []tiles;
    }
    void Clear();
    void SetTile(int x, int y, TileState newState);

    int tilesOnMapVertical;
    int tilesOnMapHorizontal;

private:
    TileState* tiles{ nullptr };
    int windowWidth;
    int windowHeight;

    friend class PrototypeLevel;
};
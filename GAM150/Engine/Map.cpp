#include "Map.h"
#include "../Game/PrototypeLevel.h"

Map::Map(int winWidth, int winHeight) : windowWidth(winWidth), windowHeight(winHeight)
    , tilesOnMapHorizontal{ winWidth / TILE_WIDTH }, tilesOnMapVertical{ winHeight / TILE_HEIGHT }
    , tiles(new TileState[static_cast<size_t>(tilesOnMapHorizontal)* tilesOnMapVertical]){}

void Map::Clear()
{
    for (int y{}; y < tilesOnMapVertical; ++y) 
    {
        for (int x{}; x < tilesOnMapHorizontal; ++x) 
        {
            tiles[y * tilesOnMapHorizontal + x] = TileState::BLANK;
        }
    }
}

void Map::SetTile(int x, int y, TileState newState)
{
    if (x < 0 || y < 0 || x >= Map::tilesOnMapHorizontal || y >= Map::tilesOnMapVertical) 
    {
        //throw std::runtime_error("Map index out of range");
        return;
    }
    tiles[y * Map::tilesOnMapHorizontal + x] = newState;
}

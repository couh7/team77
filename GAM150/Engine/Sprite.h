#pragma once
#include <string>   // std::string
#include "Texture.h"// Texture, Vector2D, Vector2DInt, TransformMatrix

class Sprite 
{
	Texture texture;
	std::vector<Vector2DInt> hotspots;
public:
	Sprite();
	void Load(std::string texturePath);
	void Draw(TransformMatrix);
	void  AddHotSpot(Vector2DInt position);
	Vector2DInt GetHotSpot();			//main hotspot
	Vector2DInt GetHotSpot(int index);
	Vector2DInt GetSize();
};
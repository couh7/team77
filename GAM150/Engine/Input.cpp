#include "Input.h" 
#include "Engine.h"

Input::Input() {
    keyDown.resize(static_cast<int>(KeyboardButton::Count));
    wasKeyDown.resize(static_cast<int>(KeyboardButton::Count));
}

bool Input::InputKey::IsKeyDown() const
{
    return Engine::GetInput().IsKeyDown(button);
}
bool Input::InputKey::IsKeyReleased() const
{
    return Engine::GetInput().IsKeyReleased(button);
}
void Input::InputKey::Reassign(Input::KeyboardButton key)
{
   InputKey::button = key;
}

bool Input::IsKeyDown(KeyboardButton key) const {
    return keyDown[static_cast<int>(key)];
}

bool Input::IsKeyUp(KeyboardButton key) const {
    return keyDown[static_cast<int>(key)] == false;
}

bool Input::IsKeyReleased(KeyboardButton key) const {
    return keyDown[static_cast<int>(key)] == false && wasKeyDown[static_cast<int>(key)] == true;
}

void Input::SetKeyDown(KeyboardButton key, bool value) {
    keyDown[static_cast<int>(key)] = value;
}

void Input::Update() {
    wasKeyDown = keyDown;
}
#include "doodle/drawing.hpp"  //doodle::Image, draw_image
#include "Texture.h"

Texture::Texture()
{
}

Texture::Texture(const std::filesystem::path& filePath):image(filePath)
{
	size = { image.GetWidth(),image.GetHeight() };
}

void Texture::Load(const std::filesystem::path& pilePath)
{
	image = doodle::Image{ pilePath };
	size = { image.GetWidth(),image.GetHeight() };
}
void Texture::Draw(TransformMatrix displayMatrix)
{
	doodle::push_settings();
	doodle::apply_matrix(displayMatrix[0][0], displayMatrix[1][0], displayMatrix[0][1],
						 displayMatrix[1][1], displayMatrix[0][2], displayMatrix[1][2]);
	doodle::draw_image(image, 0, 0);
	doodle::pop_settings();
}
Vector2DInt Texture::GetSize() { return size; }
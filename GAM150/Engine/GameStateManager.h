#pragma once
#include <vector>
#include "..\Engine\GameState.h"

class GameStateManager {
public:
	GameStateManager();

	void AddGameState(GameState& gameState);
	void Update(double);
	void SetNextState(int initState);
	void Shutdown();
	void ReloadState();
	bool HasGameEnded() { return state == State::EXIT; }

private:
	enum class State {
		START,
		LOAD,
		RUNNING,
		UNLOAD,
		SHUTDOWN,
		EXIT,
	};

	std::vector<GameState*> gameStates;
	State state;
	GameState* currGameState;
	GameState* nextGameState;
};


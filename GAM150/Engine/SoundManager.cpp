#include "SoundManager.h"
#include <iostream>

void SoundManager::BackgroundMusic(std::string filename) {
	sf::Music bgm;
	if (!bgm.openFromFile(filename)) {
		std::cerr << "Failed to load the background music file: " + filename << "\n";
	}

	bgm.setLoop(true);
	bgm.play();
	std::cout << "now play\n";
}

void SoundManager::LoadEffectMusic(std::string filename) {
	SoundBuffers.emplace_back();
	sf::SoundBuffer& buffer = SoundBuffers.back();
	if (!buffer.loadFromFile(filename)) {
		std::cerr << "Failed to load the background music file: " + filename << "\n";
	}
}

void SoundManager::PlayEffectMusic(int buffer_index) {
	for (auto& sound : Sounds)
	{
		if (sound.getStatus() != sf::SoundSource::Playing)
		{
			sound.setBuffer(SoundBuffers[buffer_index]);
			sound.play();
			return;
		}
	}
	Sounds.emplace_back(SoundBuffers[buffer_index]);
	Sounds.back().play();
}

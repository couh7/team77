#pragma once

#include "SFML/Audio.hpp"

class SoundManager {
public:
	void BackgroundMusic(std::string filename);
	void LoadEffectMusic(std::string filename);
	void PlayEffectMusic(int buffer_index);

private:
	std::vector<sf::SoundBuffer> SoundBuffers{};
	std::vector<sf::Sound>       Sounds{};
};

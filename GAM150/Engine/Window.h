#pragma once
#include"BasicDataTypes.h" // Vector2DInt

class Window {
public:
    void Init(std::string windowName);
    void Resize(int newWidth, int newHeight);
    Vector2DInt GetSize();
    void Clear(Color color);
    void Update();
private:
    Vector2DInt screenSize;
};
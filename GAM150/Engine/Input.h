#pragma once
#include <vector>

class Input {
public:
	enum class KeyboardButton {
		None,	Enter,	Escape, Space, 	Left,	Up,		Right,	Down,
		A,		B,		C,		D,		E,		F,		G,		H,		I,		J,
		K,		L,		M,		N,		O,		P,		Q,		R,		S,		T,
		U,		V,		W,		X,		Y,		Z,
		Count
	};
	class InputKey 
	{
		Input::KeyboardButton button;
		public:
			InputKey(Input::KeyboardButton button):button(button) {}
			bool IsKeyDown() const;
			bool IsKeyReleased() const;
			void Reassign(Input::KeyboardButton key);
	};
	Input();
	bool IsKeyDown(KeyboardButton key) const;
	bool IsKeyUp(KeyboardButton key) const;
	bool IsKeyReleased(KeyboardButton key) const;
	void SetKeyDown(KeyboardButton key, bool value);
	void Update();
private:
	std::vector<bool> keyDown;
	std::vector<bool> wasKeyDown;
};


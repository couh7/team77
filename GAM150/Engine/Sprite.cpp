#include "Engine.h"			  // Engine::GetWindow()
#include "doodle/drawing.hpp" // doodle clear background
#include "Sprite.h"


Sprite::Sprite() {}
void Sprite::Load(std::string texturePath)
{
	texture.Load(texturePath);
}
void  Sprite::AddHotSpot(Vector2DInt position) 
{
	hotspots.push_back(position);
}
void Sprite::Draw(TransformMatrix displayMatrix)
{
	texture.Draw(displayMatrix*TranslateMatrix(-hotspots[0]));
}
Vector2DInt Sprite::GetHotSpot()
{
	return hotspots[0];
}
Vector2DInt Sprite::GetHotSpot(int index)
{
	return hotspots[index];//std::vector, range error
}

Vector2DInt Sprite::GetSize() 
{
	return texture.GetSize();
}
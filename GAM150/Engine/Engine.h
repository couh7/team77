#include <chrono>                //std::chrono::system_clock::time_point
#include "GameStateManager.h"
#include "Input.h"
#include "Window.h"


class Engine {
public:
	static Engine& Instance() { static Engine instance; return instance; }
	static Input& GetInput() { return Instance().input; }
	static Window& GetWindow() { return Instance().window; }
	static GameStateManager& GetGameStateManager() { return Instance().gameStateManager; }
	
	void Init(std::string windowName);
	void Shutdown();
	void Update();
	bool HasGameEnded();

	const static double targetFPS;
private:
	Engine();
	~Engine();

	GameStateManager gameStateManager;
	Input input;
	Window window;

	std::chrono::system_clock::time_point lastTick;
};
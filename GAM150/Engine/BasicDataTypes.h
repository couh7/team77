#pragma once
#include <stdexcept> //std::runtime_error

typedef unsigned Color;

struct [[nodiscard]] Vector2D
{
	double x{0.0};
	double y{0.0};
public:
	constexpr Vector2D operator+(const Vector2D& vec) const noexcept;
	constexpr Vector2D operator-(const Vector2D& vec) const noexcept;
	constexpr Vector2D operator*(double scalar) const noexcept;
	constexpr Vector2D operator/(double scalar) const;
	constexpr void operator+=(const Vector2D& vec) noexcept;
	constexpr void operator-=(const Vector2D& vec) noexcept;
	constexpr Vector2D operator-() const noexcept;
};
constexpr Vector2D Vector2D::operator+(const Vector2D& vec) const noexcept
{
	return Vector2D{ x + vec.x, y + vec.y };
}
constexpr Vector2D Vector2D::operator-(const Vector2D& vec) const noexcept
{
	return Vector2D{ x - vec.x, y - vec.y };
}
constexpr Vector2D Vector2D::operator*(double scalar) const noexcept
{
	return Vector2D{ x * scalar, y * scalar };
}
constexpr Vector2D Vector2D::operator/(double scalar) const
{
	if (scalar == 0)
	{
		throw std::runtime_error("cannot divide by zero");
	}
	return Vector2D{ x / scalar, y / scalar };
}
constexpr void Vector2D::operator+=(const Vector2D& vec) noexcept
{
	x += vec.x;
	y += vec.y;
}
constexpr void Vector2D::operator-=(const Vector2D& vec) noexcept
{
	x -= vec.x;
	y -= vec.y;
}
constexpr Vector2D Vector2D::operator-() const noexcept
{
	return Vector2D{ -x, -y };
}

struct [[nodiscard]] Vector2DInt
{
	int x{0};
	int y{0};
public:
	constexpr Vector2DInt operator+(const Vector2DInt& vec) const noexcept;
	constexpr Vector2DInt operator-(const Vector2DInt& vec) const noexcept;
	constexpr Vector2DInt operator*(int scalar) const noexcept;
	constexpr Vector2DInt operator/(int scalar) const;
	constexpr void operator+=(const Vector2DInt& vec) noexcept;
	constexpr void operator-=(const Vector2DInt& vec) noexcept;
	constexpr Vector2DInt operator-() const noexcept;
	inline operator Vector2D() const noexcept;
};

constexpr Vector2DInt Vector2DInt::operator+(const Vector2DInt& vec) const noexcept
{
	return Vector2DInt{ x + vec.x, y + vec.y };
}
constexpr Vector2DInt Vector2DInt::operator-(const Vector2DInt& vec) const noexcept
{
	return Vector2DInt{ x - vec.x, y - vec.y };
}
constexpr Vector2DInt Vector2DInt::operator*(int scalar) const noexcept
{
	return Vector2DInt{ x * scalar, y * scalar };
}
constexpr Vector2DInt Vector2DInt::operator/(int scalar) const
{
	if (scalar == 0)
	{
		throw std::runtime_error("cannot divide by zero");
	}
	return Vector2DInt{ x / scalar, y / scalar };
}
constexpr void Vector2DInt::operator+=(const Vector2DInt& vec) noexcept
{
	x += vec.x;
	y += vec.y;
}
constexpr void Vector2DInt::operator-=(const Vector2DInt& vec) noexcept
{
	x -= vec.x;
	y -= vec.y;
}
constexpr Vector2DInt Vector2DInt::operator-() const noexcept
{
	return Vector2DInt{ -x, -y };
}
inline Vector2DInt::operator Vector2D() const noexcept
{
	return Vector2D{ static_cast<double>(x),static_cast<double>(y) };
}

struct Rect 
{
	Vector2D bottomLeft;
	Vector2D topRight;
};
#include "Engine.h"
#include "GameStateManager.h"

GameStateManager::GameStateManager() : currGameState(nullptr), nextGameState(nullptr), state(State::START) { }

void GameStateManager::AddGameState(GameState& gameState) {
	gameStates.push_back(&gameState);
}

void GameStateManager::Update(double deltaTime)
{
	switch (state) 
	{
	case State::START:
		SetNextState(0); 
		state = State::LOAD;
		break;

	case State::LOAD:
		currGameState = nextGameState;
		currGameState->Load();
		state = State::RUNNING;
		break;

	case State::RUNNING:
		if (currGameState == nextGameState) 
		{
			currGameState->Update(deltaTime);
			currGameState->Draw();
		}
		else
		{
			state = State::UNLOAD;
		}
		break;

	case State::UNLOAD:
		currGameState->Unload();
		if (nextGameState == nullptr) 
		{
			state = State::SHUTDOWN;
		}
		else 
		{
			state = State::LOAD;
		}
		break;

	case State::SHUTDOWN:
		state = State::EXIT;
		break;

	case State::EXIT:
		break;
	}
}

void GameStateManager::SetNextState(int initState) {
	nextGameState = gameStates[initState];
}

void GameStateManager::ReloadState() {
	state = State::UNLOAD;
}

void GameStateManager::Shutdown() {
	state = State::UNLOAD;
	nextGameState = nullptr;
}

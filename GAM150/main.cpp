#include <exception>	// std::exception
#include <iostream>		//cerr

#include "Engine/Engine.h"
#include "Game/Screens.h"
#include "Game/RetrySplash.h"
#include "Game/PrototypeLevel.h"
#include "Game/CompleteSplash.h"
//#include "SFML/Audio.hpp"
#include"Engine/SoundManager.h"

int main(void) {
	Engine& engine = Engine::Instance();
	//SoundManager sound;
	sf::Music bgm;
	if (!bgm.openFromFile("assets/test.ogg")) {
		std::cerr << "Failed to load the background music file: assets/test.ogg" << "\n";
	}

	bgm.setLoop(true);
	bgm.play();
	std::cout << "now play\n";

	try {
		engine.Init("TopViewPuzzle");
		PrototypeLevel prototypeLevel;
		RetrySplash retrySplash;
		CompleteSplash completeSplash;

		//sound.BackgroundMusic("assets/test.ogg");

		engine.GetGameStateManager().AddGameState(prototypeLevel);
		engine.GetGameStateManager().AddGameState(retrySplash);
		engine.GetGameStateManager().AddGameState(completeSplash);

		while (engine.HasGameEnded() == false) {
			engine.Update();
		}
		engine.Shutdown();
		return 0;
	} catch (std::exception & e) {
		std::cerr << e.what() << std::endl;
		return -1;
	}
}
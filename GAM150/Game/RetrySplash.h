#pragma once
#include "..\Engine\GameState.h"    //gameState
#include "..\Engine\Texture.h"      //texture
#include "..\Engine\Input.h"        //input

class RetrySplash : public GameState
{
    Texture texture;
    double timer{ 0.0 };
    Input::InputKey nextLevel;

public:
    RetrySplash();
    void Load() override;
    void Update(double dt) override;
    void Unload() override;
    void Draw() override;
    std::string GetName() override { return "Retry splash"; }
};
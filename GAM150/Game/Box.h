#pragma once
#include "../Engine/Sprite.h" // Sprite

class Box
{
	bool hasDisabled;
	int currTileX, currTileY;
	int prevTileX, prevTileY;

	Sprite sprite;
	Vector2DInt initTilePosition;
	Vector2D position;
	TransformMatrix boxMatrix;

	friend class PrototypeLevel;
public:
	double boxXScale;
	double boxYScale;

	Box(int,int);

	void Load();
	void Update();
	void Draw();
	Vector2D GetPosition();

	bool TestForBorderTopDown();
	bool TestForBorderLeftRight();
};
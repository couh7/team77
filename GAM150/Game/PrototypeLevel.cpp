#include "PrototypeLevel.h"
#include "Screens.h" //CompleteSplash,RetrySplash
#include "../Engine/Engine.h" //Engine::GetLogger()
#include "../Engine/SoundManager.h"

const double PrototypeLevel::targetFPS = 60.0;

SoundManager effectSound;

PrototypeLevel::PrototypeLevel()
	:map(Engine::GetWindow().GetSize().x, Engine::GetWindow().GetSize().y)
	, mouse(3, 3), cheese(20, 15)
	, levelReload(Input::KeyboardButton::R), quitKey(Input::KeyboardButton::Escape) {}

void PrototypeLevel::Load()
{
	map.Clear();
	mouse.Load();
	cheese.Load();
	traps.emplace_back(Trap(20, 16));
	traps.emplace_back(Trap(20, 14));
	traps.emplace_back(Trap(19, 15));
	traps.emplace_back(Trap(21, 15));
	boxes.emplace_back(Box(10, 10));
	boxes.emplace_back(Box(10, 11));
	boxes.emplace_back(Box(9, 10));
	for (int i{ 0 }; i < boxes.size(); ++i) {
		boxes[i].Load();
	}
	for (int i{ 0 }; i < traps.size(); ++i) {
		traps[i].Load();
	}
	effectSound.LoadEffectMusic("assets/test1.ogg");
}
void PrototypeLevel::Update(double)
{
	mouse.Update();
	cheese.Update();
	for (Trap& trap : traps) {
		trap.Update();
	}
	for (Box& box : boxes) {
		box.Update();
	}

	//collision
	if (mouse.hasMoved() == true)
	{
		//mouse <-> box
		Map::TileState currTile = map.tiles[map.tilesOnMapHorizontal * mouse.GetCurrTileY() + mouse.GetCurrTileX()];
		if (currTile == Map::TileState::BOX)
		{
			for (int i{}; i < boxes.size(); ++i)
			{
				//mouse<->box, collision
				if (boxes[i].currTileX == mouse.currTileX &&
					boxes[i].currTileY == mouse.currTileY)
				{

					int dx = mouse.GetCurrTileX() - mouse.GetPrevTileX();
					int dy = mouse.GetCurrTileY() - mouse.GetPrevTileY();

					if (boxes[i].TestForBorderLeftRight() == true)//box out of border(left or right)
					{
						boxes[i].prevTileY = boxes[i].currTileY;
						boxes[i].currTileY = mouse.GetCurrTileY() + dy;
						mouse.currTileX = mouse.prevTileX;

						//box <-> box collision
						Map::TileState boxTile = map.tiles[map.tilesOnMapHorizontal * boxes[i].currTileY + boxes[i].currTileX];
						if (boxTile == Map::TileState::BOX)
						{
							mouse.currTileX = mouse.prevTileX;
							mouse.currTileY = mouse.prevTileY;
							boxes[i].currTileY = boxes[i].prevTileY;
						}
						break;
					}
					if (boxes[i].TestForBorderTopDown() == true)//box out of border(top or bottom)
					{
						boxes[i].prevTileX = boxes[i].currTileX;
						boxes[i].currTileX = mouse.GetCurrTileX() + dx;
						mouse.currTileY = mouse.prevTileY;

						//box , box collision
						Map::TileState boxTile = map.tiles[map.tilesOnMapHorizontal * boxes[i].currTileY + boxes[i].currTileX];
						if (boxTile == Map::TileState::BOX)
						{
							mouse.currTileX = mouse.prevTileX;
							mouse.currTileY = mouse.prevTileY;
							boxes[i].currTileX = boxes[i].prevTileX;
						}
						break;
					}

					//update box tile position
					if (boxes[i].TestForBorderLeftRight() == false && boxes[i].TestForBorderTopDown() == false)
					{
						boxes[i].prevTileX = boxes[i].currTileX;
						boxes[i].prevTileY = boxes[i].currTileY;
						boxes[i].currTileX = mouse.GetCurrTileX() + dx;
						boxes[i].currTileY = mouse.GetCurrTileY() + dy;
					}

					Map::TileState boxTile = map.tiles[map.tilesOnMapHorizontal * boxes[i].currTileY + boxes[i].currTileX];
					//box <-> trap collision
					if (boxTile == Map::TileState::TRAP)
					{
						for (int j{ 0 }; j < traps.size(); ++j)
						{
							//same position
							if (traps[j].currTileX == boxes[i].currTileX &&
								traps[j].currTileY == boxes[i].currTileY)
							{
								map.tiles[map.tilesOnMapHorizontal * boxes[i].currTileY + boxes[i].currTileX] = Map::TileState::BLANK;
								traps.erase(traps.begin() + j);
								boxes.erase(boxes.begin() + i);
								break;
							}
						}
						//TODO :: trap breaking sound 
						break;
					}

					//box <-> box collision
					else if (boxTile == Map::TileState::BOX)
					{
						mouse.currTileX = mouse.prevTileX;
						mouse.currTileY = mouse.prevTileY;
						boxes[i].currTileX = boxes[i].prevTileX;
						boxes[i].currTileY = boxes[i].prevTileY;
						break;
					}
					effectSound.PlayEffectMusic(0);
				}
			}
		}
		//mouse<->trap
		else if (currTile == Map::TileState::TRAP)
		{
			Unload();
			Engine::GetGameStateManager().SetNextState(RETRY_SPLASH);
		}
		else if (currTile == Map::TileState::CHEESE) 
		{
			Unload();
			Engine::GetGameStateManager().SetNextState(COMPLETE_SPLASH);
		}
	}

	//set mouse matrix again (after collision)
	mouse.mouseMatrix = TranslateMatrix(mouse.position) * RotateMatrix(mouse.rotationAngle) * ScaleMatrix({ mouse.mouseXScale,mouse.mouseYScale });

	//set tiles
	map.tiles[map.tilesOnMapHorizontal * mouse.currTileY + mouse.currTileX] = Map::TileState::MOUSE;
	map.tiles[map.tilesOnMapHorizontal * cheese.currTileY + cheese.currTileX] = Map::TileState::CHEESE;
	for (Box& box : boxes) {
		map.tiles[map.tilesOnMapHorizontal * box.currTileY + box.currTileX] = Map::TileState::BOX;
	}
	for (Trap& trap : traps) {
		map.tiles[map.tilesOnMapHorizontal * trap.GetCurrTileY() + trap.GetCurrTileX()] = Map::TileState::TRAP;
	}

	//reset level
	if (levelReload.IsKeyReleased() == true)
	{
		Unload();
		Load();
	}

	//escape level
	if (quitKey.IsKeyReleased() == true)
	{
		Engine::GetGameStateManager().Shutdown();
	}
}
void PrototypeLevel::Unload()
{
	traps.clear();
	boxes.clear();
}

void PrototypeLevel::Draw()
{
	Engine::GetWindow().Clear(0xFFffFFff);
	mouse.Draw();
	cheese.Draw();
	for (Box& box : boxes) {
		box.Draw();
	}
	for (Trap& trap : traps) {
		trap.Draw();
	}
}
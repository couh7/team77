/*--------------------------------------------------------------
Copyright (C) 2020 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name: Level1.h
Purpose: Header file for Level 1
Project: CS230
Author: byeonggyu Park
Creation date: 4/15/2020
-----------------------------------------------------------------*/
#pragma once
#include "Hero.h"			           // Hero
#include "Ball.h"					   // Ball 1,2,3
#include "Background.h"				   // Background::backgrounds
#include "..\Engine\Input.h"           // Input::InputKey
#include "..\Engine\GameState.h"

class Level1 : public GameState 
{
	Hero hero;
	Ball ball1,ball2,ball3;
	Vector2D cameraPos;
	Background background;
	TransformMatrix cameraMatrix;

	Input::InputKey levelReload;
	Input::InputKey levelNext;

public:
	static const double floor;
	static const double gravity;
	static const double cameraPlayerMoveRange;

	Level1();
	void Load() override;
	void Update(double) override;
	void Unload() override;
	void Draw() override;
	std::string GetName() override { return "Level 1"; }

};
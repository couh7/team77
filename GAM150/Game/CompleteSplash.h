#include "..\Engine\GameState.h"    //gameState
#include "..\Engine\Texture.h"      //texture
#include "..\Engine\Input.h"        //Input

class CompleteSplash : public GameState 
{
    Texture texture;
    double timer{ 0.0 };
    Input::InputKey nextLevel;

public:
    CompleteSplash();
    void Load() override;
    void Update(double dt) override;
    void Unload() override;
    void Draw() override;
    std::string GetName() override { return "Complete splash"; }
};
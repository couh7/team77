#include "RetrySplash.h"
#include "Screens.h"           //PROTOTYPE_LEVEL
#include "..\Engine\Engine.h" //GetGameStateManager()
RetrySplash::RetrySplash() :nextLevel(Input::KeyboardButton::Enter) {}
void RetrySplash::Load()
{
    texture.Load("assets/mouse_trapped.jpg");
}
void RetrySplash::Update(double dt)
{
    timer += dt;
    if (timer > 3.0 ||
        nextLevel.IsKeyReleased() == true)
    {
        timer = 0.0;
        Engine::GetGameStateManager().SetNextState(PROTOTYPE_LEVEL);
    }
}
void RetrySplash::Unload() {}
void RetrySplash::Draw()
{
    Engine::GetWindow().Clear(0xffFFffFF);
    texture.Draw(TranslateMatrix({ (Engine::GetWindow().GetSize().operator Vector2D() - texture.GetSize())/ 2.0 }));
}
/*--------------------------------------------------------------
Copyright (C) 2020 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name: Level1.cpp
Purpose: Source file for Level 1
Project: CS230
Author: byeonggyu Park
Creation date: 4/15/2020
-----------------------------------------------------------------*/
#include "..\Engine\Engine.h" // Engine::GetLogger()
#include "Screens.h"          // LEVEL2
#include "Level1.h"           

const double Level1::floor = 127.0;
const double Level1::gravity = 1600.0;
const double Level1::cameraPlayerMoveRange = 0.35;

Level1::Level1()
	:cameraPos{ 0.0,0.0 },
	hero({ 100.0,Level1::floor },cameraPos),
	ball1({ 600.0,Level1::floor }),
	ball2({ 2700.0,Level1::floor }),
	ball3({ 4800.0,Level1::floor }),
	levelReload(Input::KeyboardButton::R), levelNext(Input::KeyboardButton::Enter){}

void Level1::Load()
{
	cameraPos = { 0.0,0.0 };

	hero.Load();
	ball1.Load();
	ball2.Load();
	ball3.Load();
	background.Add("New_assets/clouds.png",4);
	background.Add("New_assets/Moutains.png", 2);
	background.Add("New_assets/foreground.png", 1);
}
void Level1::Update(double dt)
{
	hero.Update(dt);

	ball1.Update(dt);
	ball2.Update(dt);
	ball3.Update(dt);

	double cameraDistance = Engine::GetWindow().GetSize().x * Level1::cameraPlayerMoveRange;//35% left of the window
	if (hero.GetPosition().x-cameraPos.x > cameraDistance)
	{
		cameraPos.x = hero.GetPosition().x - cameraDistance;
	}

	if (cameraPos.x < background.GetCameraRange().bottomLeft.x) 
	{
		cameraPos.x = background.GetCameraRange().bottomLeft.x;
	}
	else if (cameraPos.x > background.GetCameraRange().topRight.x)
	{
		cameraPos.x = background.GetCameraRange().topRight.x;
	}
	if (cameraPos.y < background.GetCameraRange().bottomLeft.y)
	{
		cameraPos.y = background.GetCameraRange().bottomLeft.y;
	}
	else if (cameraPos.y > background.GetCameraRange().topRight.y)
	{
		cameraPos.y = background.GetCameraRange().topRight.y;
	}

	if (levelReload.IsKeyReleased()==true)
	{
		Level1::Load();
	}

	if (levelNext.IsKeyReleased()==true) 
	{
		Engine::GetGameStateManager().SetNextState(LEVEL2);
	}

	cameraMatrix = TranslateMatrix(-cameraPos);
}
void Level1::Unload()
{
	background.Unload();
}
void Level1::Draw() 
{
	Engine::GetWindow().Clear(0x3399DAFF);//in case needed
	background.Draw(cameraPos);

	ball1.Draw(cameraMatrix);
	ball2.Draw(cameraMatrix);
	ball3.Draw(cameraMatrix);

	hero.Draw(cameraMatrix);
}
#include "CompleteSplash.h"
#include "Screens.h"           //PROTOTYPE_LEVEL
#include "..\Engine\Engine.h" //GetGameStateManager()
CompleteSplash::CompleteSplash() :nextLevel(Input::KeyboardButton::Enter){}
void CompleteSplash::Load() 
{
    texture.Load("assets/mouse_eating_cheese.jpg");
}
void CompleteSplash::Update(double dt) 
{
    timer += dt;
    if (timer > 3.0 || 
        nextLevel.IsKeyReleased() == true)
    {
        timer = 0.0;
        Engine::GetGameStateManager().SetNextState(PROTOTYPE_LEVEL);
    }
}
void CompleteSplash::Unload() {}
void CompleteSplash::Draw() 
{
    Engine::GetWindow().Clear(0xffFFffFF);
    texture.Draw(TranslateMatrix({ (Engine::GetWindow().GetSize().operator Vector2D() - texture.GetSize()) / 2.0 }));
}
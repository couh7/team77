#include "Mouse.h"
#include "../Engine/Engine.h"  //GetWindow();

Mouse::Mouse(int tileNumberX,int tileNumberY)
	:initTilePosition({ tileNumberX,tileNumberY })
	,rotationAngle{0.0},currTileX(tileNumberX),currTileY(tileNumberY),prevTileX(currTileX),prevTileY(currTileY)
	, moveRightKey(Input::KeyboardButton::A), moveLeftKey(Input::KeyboardButton::D),mouseXScale(1.0),mouseYScale(1.0)
	, moveUpKey(Input::KeyboardButton::W), moveDownKey(Input::KeyboardButton::S) {}

void Mouse::Load()
{
	sprite.Load("assets/mouse.png");
	sprite.AddHotSpot(Vector2DInt{sprite.GetSize()/2});
	prevTileX=currTileX = initTilePosition.x;
	prevTileY = currTileY = initTilePosition.y;

	mouseXScale = TILE_WIDTH / static_cast<double>(sprite.GetSize().x);
	mouseYScale = TILE_HEIGHT / static_cast<double>(sprite.GetSize().y);
}
void Mouse::Update()
{
	if (moveLeftKey.IsKeyReleased()==true)
	{
		rotationAngle = -doodle::HALF_PI;

		prevTileX = currTileX;
		prevTileY = currTileY;
		currTileX++;
	}
	else if (moveRightKey.IsKeyReleased()==true)
	{
		rotationAngle = doodle::HALF_PI;

		prevTileX = currTileX;
		prevTileY = currTileY;
		currTileX--;
	}

	if (moveUpKey.IsKeyReleased()==true) 
	{
		rotationAngle = 0.0;
		if (moveLeftKey.IsKeyReleased() == true) { rotationAngle -= doodle::QUARTER_PI; }
		else if (moveRightKey.IsKeyReleased() == true) { rotationAngle += doodle::QUARTER_PI; }

		prevTileX = currTileX;
		prevTileY = currTileY;
		currTileY++;

	}
	else if (moveDownKey.IsKeyReleased() == true)
	{
		rotationAngle = -doodle::PI;
		if (moveLeftKey.IsKeyReleased() == true) { rotationAngle += doodle::QUARTER_PI; }
		else if (moveRightKey.IsKeyReleased() == true) { rotationAngle -= doodle::QUARTER_PI; }

		prevTileX = currTileX;
		prevTileY = currTileY;
		currTileY--;
	}
	position.x = currTileX * static_cast<double>(TILE_WIDTH);
	position.y = currTileY * static_cast<double>(TILE_HEIGHT);

	TestForBorder();
}

void Mouse::Draw()
{
	sprite.Draw(mouseMatrix);
}

void Mouse::TestForBorder()
{
	const Vector2DInt windowSize = Engine::GetWindow().GetSize();
	const Vector2DInt mouseSize = sprite.GetSize();
	const double mouseWidthHalf = (mouseSize.x*mouseXScale)/2.0;
	const double mouseHeightHalf = (mouseSize.y*mouseYScale)/2.0;

	if (position.x+mouseWidthHalf > windowSize.x)
	{
		position.x =windowSize.x-mouseWidthHalf;
	}
	else if (position.x-mouseWidthHalf < 0)
	{
		position.x = mouseWidthHalf;
	}
	if (position.y + mouseHeightHalf > windowSize.y) 
	{
		position.y = windowSize.y-mouseHeightHalf;
	}
	else if (position.y-mouseHeightHalf< 0)
	{
		position.y=mouseHeightHalf;
	}
}

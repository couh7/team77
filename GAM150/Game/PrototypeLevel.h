#pragma once

#include <chrono>					   //system_clock::time_point;

#include "Box.h"
#include "Mouse.h"			           // Mouse
#include "Cheese.h"	//cheese
#include "Trap.h"	//trap
#include "..\Engine\Input.h"           // Input::InputKey
#include "..\Engine\GameState.h"
#include "..\Engine\Map.h"

class PrototypeLevel : public GameState
{
	Map map;
	Mouse mouse;
	Cheese cheese;

	std::vector<Trap> traps;
	std::vector<Box> boxes;

	Input::InputKey levelReload;
	Input::InputKey quitKey;

	friend class Map;

	const static double targetFPS;
	std::chrono::system_clock::time_point lastTick;
public:
	PrototypeLevel();
	void Load() override;
	void Update(double) override;
	void Unload() override;
	void Draw() override;
	std::string GetName() override { return "Prototype level"; }
};

#pragma once
#include "../Engine/Sprite.h" // Sprite

class Cheese
{
	int currTileX, currTileY;
	Sprite sprite;
	Vector2DInt initPosition;
	Vector2D position;
	TransformMatrix cheeseMatrix;

	friend class PrototypeLevel;

public:
	double cheeseXScale;
	double cheeseYScale;

	Cheese(int, int);

	void Load();
	void Update();
	void Draw();
	Vector2D GetPosition();
};
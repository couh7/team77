#pragma once
#include "../Engine/Sprite.h" // Sprite

class Trap
{
	int currTileX, currTileY;
	int prevTileX, prevTileY;

	Sprite sprite;
	Vector2DInt initTilePosition;
	Vector2D position;
	TransformMatrix trapMatrix;

	friend class PrototypeLevel;

public:
	double trapXScale;
	double trapYScale;

	Trap(int, int);

	void Load();
	void Update();
	void Draw();
	Vector2D GetPosition();

	[[nodiscard]] int GetCurrTileX()const noexcept { return currTileX; }
	[[nodiscard]] int GetCurrTileY()const noexcept { return currTileY; }
	[[nodiscard]] int GetPrevTileX()const noexcept { return prevTileX; }
	[[nodiscard]] int GetPrevTileY()const noexcept { return prevTileY; }

};
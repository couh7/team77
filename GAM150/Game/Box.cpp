#include "Box.h"
#include "Mouse.h"
#include "../Engine/Engine.h"  //GetWindow();

Box::Box(int tileX,int tileY)
	:currTileX(tileX), currTileY(tileY),prevTileX(tileX),prevTileY(tileY),hasDisabled(false)
	, initTilePosition({ tileX,tileY }),boxXScale(1.0),boxYScale(1.0){}

void Box::Load()
{
	sprite.Load("assets/box.png");
	sprite.AddHotSpot(Vector2DInt{ Box::sprite.GetSize()/2});
	currTileX = initTilePosition.x;
	currTileY = initTilePosition.y;

	boxXScale = TILE_WIDTH / static_cast<double>(sprite.GetSize().x);
	boxYScale = TILE_HEIGHT / static_cast<double>(sprite.GetSize().y);

	position.x = currTileX * static_cast<double>(TILE_WIDTH);
	position.y = currTileY * static_cast<double>(TILE_HEIGHT);
	boxMatrix = TranslateMatrix(position) * ScaleMatrix({ boxXScale,boxYScale });
}
void Box::Update()
{
	position.x = currTileX * static_cast<double>(TILE_WIDTH);
	position.y = currTileY * static_cast<double>(TILE_HEIGHT);

	boxMatrix = TranslateMatrix(position) * ScaleMatrix({ boxXScale,boxYScale });
}

bool Box::TestForBorderTopDown() 
{
	bool hasChanged{ false };
	const Vector2DInt windowSize = Engine::GetWindow().GetSize();
	const Vector2DInt boxSize = sprite.GetSize();
	const double boxHeightHalf = (boxSize.y * boxYScale) / 2.0;
	if (position.y + boxHeightHalf > windowSize.y)
	{
		position.y = windowSize.y - boxHeightHalf;
		hasChanged = true;
	}
	else if (position.y - boxHeightHalf < 0)
	{
		position.y = boxHeightHalf;
		hasChanged = true;
	}
	return hasChanged;
}

bool Box::TestForBorderLeftRight()
{
	bool hasChanged{ false };

	const Vector2DInt windowSize = Engine::GetWindow().GetSize();
	const Vector2DInt boxSize = sprite.GetSize();
	const double boxWidthHalf = (boxSize.x * boxXScale) / 2.0;

	if (position.x + boxWidthHalf > windowSize.x)
	{
		position.x = windowSize.x - boxWidthHalf;
		hasChanged = true;
	}
	else if (position.x - boxWidthHalf < 0)
	{
		position.x = boxWidthHalf;
		hasChanged = true;
	}

	return hasChanged;
}
void Box::Draw()
{
	if (hasDisabled==false) {
	Box::sprite.Draw(boxMatrix);
	}
}

Vector2D Box::GetPosition()
{
	return position;
}

#include "Trap.h"
#include "../Engine/Map.h"	   // TILE_WIDTH,TILE_HEIGHT
#include "../Engine/Engine.h"  // GetWindow();

Trap::Trap(int tileX, int tileY)
	:currTileX(tileX), currTileY(tileY),prevTileX(tileX),prevTileY(tileY)
	, initTilePosition({ tileX,tileY })
	, trapXScale(1.0), trapYScale(1.0) {}

void Trap::Load()
{
	sprite.Load("assets/trap.png");
	sprite.AddHotSpot(Vector2DInt{ Trap::sprite.GetSize() / 2 });
	currTileX = initTilePosition.x;
	currTileY = initTilePosition.y;

	trapXScale = TILE_WIDTH / static_cast<double>(sprite.GetSize().x);
	trapYScale = TILE_HEIGHT / static_cast<double>(sprite.GetSize().y);

	position.x = currTileX * static_cast<double>(TILE_WIDTH);
	position.y = currTileY * static_cast<double>(TILE_HEIGHT);
	trapMatrix = TranslateMatrix(position) * ScaleMatrix({ trapXScale,trapYScale });

}
void Trap::Update()
{
	position.x = currTileX * static_cast<double>(TILE_WIDTH);
	position.y = currTileY * static_cast<double>(TILE_HEIGHT);
	trapMatrix = TranslateMatrix(position) * ScaleMatrix({ trapXScale,trapYScale });
}
void Trap::Draw()
{
	Trap::sprite.Draw(trapMatrix);
}

Vector2D Trap::GetPosition()
{
	return position;
}

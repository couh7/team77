#include "Cheese.h"
#include "../Engine/Map.h"	//TILE_WIDTH, TILE_HEIGHT
#include "../Engine/Engine.h"  //GetWindow();

Cheese::Cheese(int tileX, int tileY)
	:initPosition({ tileX,tileY }), currTileX(tileX), currTileY(tileY)
	, cheeseXScale(1.0), cheeseYScale(1.0) {}

void Cheese::Load()
{
	sprite.Load("assets/cheese.png");
	sprite.AddHotSpot(Vector2DInt{ Cheese::sprite.GetSize() / 2 });
	currTileX = initPosition.x;
	currTileY = initPosition.y;
	cheeseXScale = TILE_WIDTH / static_cast<double>(sprite.GetSize().x);
	cheeseYScale = TILE_HEIGHT / static_cast<double>(sprite.GetSize().y);
}
void Cheese::Update()
{
	position.x = currTileX * static_cast<double>(TILE_WIDTH);
	position.y = currTileY * static_cast<double>(TILE_HEIGHT);

	cheeseMatrix = TranslateMatrix(position) * ScaleMatrix({ cheeseXScale,cheeseYScale });
}
void Cheese::Draw()
{
	Cheese::sprite.Draw(cheeseMatrix);
}

Vector2D Cheese::GetPosition()
{
	return position;
}

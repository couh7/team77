#pragma once
#include "../Engine/BasicDataTypes.h" // Vector2D
#include "../Engine/Sprite.h"         // Sprite
#include "../Engine/Input.h"          // Input::InputKey
#include "../Engine/Map.h"			  // Map

class Mouse
{
public:
	Mouse(int ,int );

	void Load();
	void Update();
	void Draw();

	TransformMatrix mouseMatrix;

	friend class PrototypeLevel;
	
	[[nodiscard]] int GetCurrTileX()const noexcept { return currTileX; }
	[[nodiscard]] int GetCurrTileY()const noexcept { return currTileY; }
	[[nodiscard]] int GetPrevTileX()const noexcept { return prevTileX; }
	[[nodiscard]] int GetPrevTileY()const noexcept { return prevTileY; }
	[[nodiscard]] bool hasMoved()const noexcept { return (currTileX != prevTileX) || (currTileY != prevTileY); }

private:
	double mouseXScale;
	double mouseYScale;

	int currTileX, currTileY;
	int prevTileX, prevTileY;

	Sprite sprite;

	double rotationAngle;

	Vector2DInt initTilePosition;
	Vector2D position;

	Input::InputKey moveRightKey;
	Input::InputKey moveLeftKey;
	Input::InputKey moveUpKey;
	Input::InputKey moveDownKey;

	void TestForBorder();
};

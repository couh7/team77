#pragma once
#include <string>
#include "..\Engine\Texture.h"
#include "..\Engine\BasicDataTypes.h" //struct Rect;

class Background 
{
public:
	Background();
	void Add(const std::string& texturePath, int level);
	void Unload();
	void Draw(Vector2D camera);
	Rect GetCameraRange();
private:
	struct ParallaxInfo 
	{
		Texture texture;
		int level;
	};
	Rect cameraRange;
	std::vector<ParallaxInfo>backgrounds;
};